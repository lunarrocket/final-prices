package com.lunar.finalprices.infrastructure;

import com.lunar.finalprices.application.FindPrices;
import com.lunar.finalprices.application.ProductCriteria;
import com.lunar.finalprices.domain.BrandRepository;
import com.lunar.finalprices.domain.PricesRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@RestController
@RequestMapping("/api/v1/product")
public class GetProductController {

    private PricesRepository pricesRepository;
    private BrandRepository brandRepository;
    private FindPrices findPrices;

    public GetProductController(PricesRepository pricesRepository, BrandRepository brandRepository) {
        this.brandRepository = brandRepository;
        this.pricesRepository = pricesRepository;
        this.findPrices = new FindPrices(this.pricesRepository, this.brandRepository);
    }
    @GetMapping
    public ResponseEntity<?> invoke(ProductCriteria criteria) throws ParseException {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(findPrices.invoke(criteria));
    }
}
