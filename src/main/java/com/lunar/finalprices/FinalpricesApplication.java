package com.lunar.finalprices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalpricesApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalpricesApplication.class, args);
	}

}
