package com.lunar.finalprices.shared;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public final class ErrorResponse {
    private String message;
    private Long code;
}
