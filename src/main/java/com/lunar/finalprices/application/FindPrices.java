package com.lunar.finalprices.application;


import com.lunar.finalprices.domain.Brand;
import com.lunar.finalprices.domain.BrandRepository;
import com.lunar.finalprices.domain.PricesRepository;
import com.lunar.finalprices.domain.Product;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class FindPrices {
    private final PricesRepository pricesRepository;
    private final BrandRepository brandRepository;

    public FindPrices(PricesRepository pricesRepository, BrandRepository brandRepository) {
        this.pricesRepository = pricesRepository;
        this.brandRepository = brandRepository;
    }
    public PricesCommand invoke(ProductCriteria productCriteria) throws ParseException {
        Date date = parseDate(productCriteria.getDate());
        var product = pricesRepository
                .findByProductIdAndBrandIdAndStartDateLessThanEqualAndEndDateGreaterThanEqual(
                    productCriteria.getProduct(),
                    findBrand(productCriteria.getBrand()),
                    date,
                    date
                )
                .stream()
                .max(Comparator.comparingLong(Product::getPriority))
                .get();
        return parseToPricesCommand(product);
    }

    private PricesCommand parseToPricesCommand(Product product) {
        return PricesCommand
                .builder()
                .price(product.getPrice())
                .priceList(product.getPriceList())
                .productId(product.getProductId())
                .brandId(product.getBrandId().getBrandId())
                .startDate(product.getStartDate().toString())
                .endDate(product.getEndDate().toString())
                .build();
    }

    private Date parseDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-M-dd HH:mm:ss");
        return format.parse(date);
    }

    private Brand findBrand(Long brandId) {
        return brandRepository.findById(brandId).get();
    }
}
