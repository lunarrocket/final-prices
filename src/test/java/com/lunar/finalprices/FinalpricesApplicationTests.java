package com.lunar.finalprices;

import com.lunar.finalprices.shared.ErrorResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.lunar.finalprices.application.PricesCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
@SpringBootTest
class FinalpricesApplicationTests {

	private final String URL = "http://localhost:8080/api/v1/product?date={date}&brand={brand}&product={product}";
    private TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void endpointGetTestOne() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("product", "35455");
        uriVariables.put("brand", "1");
        uriVariables.put("date", "2020-06-14 10:00:00");
        ResponseEntity<PricesCommand> response = restTemplate.exchange(URL, HttpMethod.GET, requestEntity, PricesCommand.class, uriVariables);
        PricesCommand pricesCommand = response.getBody();
        Assertions.assertEquals(35.5, pricesCommand.getPrice());
    }

    @Test
    public void endpointGetTestTwo() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("product", "35455");
        uriVariables.put("brand", "1");
        uriVariables.put("date", "2020-06-14 16:00:00");
        ResponseEntity<PricesCommand> response = restTemplate.exchange(URL, HttpMethod.GET, requestEntity, PricesCommand.class, uriVariables);
        PricesCommand pricesCommand = response.getBody();
        Assertions.assertEquals(25.45, pricesCommand.getPrice());
    }

    @Test
    public void endpointGetTestThree() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("product", "35455");
        uriVariables.put("brand", "1");
        uriVariables.put("date", "2020-06-14 21:00:00");
        ResponseEntity<PricesCommand> response = restTemplate.exchange(URL, HttpMethod.GET, requestEntity, PricesCommand.class, uriVariables);
        PricesCommand pricesCommand = response.getBody();
        Assertions.assertEquals(35.5, pricesCommand.getPrice());
    }

    @Test
    public void endpointGetTestFour() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("product", "35455");
        uriVariables.put("brand", "1");
        uriVariables.put("date", "2020-06-15 10:00:00");
        ResponseEntity<PricesCommand> response = restTemplate.exchange(URL, HttpMethod.GET, requestEntity, PricesCommand.class, uriVariables);
        PricesCommand pricesCommand = response.getBody();
        Assertions.assertEquals(30.5, pricesCommand.getPrice());
    }

    @Test
    public void endpointGetTestFive() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("product", "35455");
        uriVariables.put("brand", "1");
        uriVariables.put("date", "2020-06-16 21:00:00");
        ResponseEntity<PricesCommand> response = restTemplate.exchange(URL, HttpMethod.GET, requestEntity, PricesCommand.class, uriVariables);
        PricesCommand pricesCommand = response.getBody();
        Assertions.assertEquals(38.95, pricesCommand.getPrice());
    }

    @Test
    public void endpointGetTestNoValuePresent() {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity requestEntity = new HttpEntity<>(headers);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("product", "55");
        uriVariables.put("brand", "1");
        uriVariables.put("date", "2020-06-16 21:00:00");
        ResponseEntity<ErrorResponse> response = restTemplate.exchange(URL, HttpMethod.GET, requestEntity, ErrorResponse.class, uriVariables);
        ErrorResponse error = response.getBody();
        Assertions.assertEquals(500, error.getCode());
        Assertions.assertEquals("No value present", error.getMessage());
    }

}
